#include "RainbowPlugin/Public/KismetNodes/SGraphNode_K2GPN.h"
#include "NodeFactory.h"
#include "RainbowPlugin/Public/BlueprintGraph/K2Node_GetPropName.h"
#include "Editor/GraphEditor/Public/SGraphPinNameList.h"

void SGraphNode_K2GPN::CreatePinWidgets()
{
	UK2Node_GetPropName* GetPropNameNode = CastChecked<UK2Node_GetPropName>(GraphNode);
	TArray<UEdGraphPin*> ValuePins = GetPropNameNode->GetAllNamePin();
	for (auto PinIt = GraphNode->Pins.CreateConstIterator(); PinIt; ++PinIt)
	{
		UEdGraphPin* CurrentPin = *PinIt;	
		if(ValuePins.Contains(CurrentPin))
		{
			TArray<TSharedPtr<FName>> RowNames;
			for(auto Name : GetPropNameNode->StructName)
			{
				RowNames.Add(MakeShareable(new FName(Name)));
			}
			TSharedPtr<SGraphPin> NewPin_2 = SNew(SGraphPinNameList, CurrentPin, RowNames);
			check(NewPin_2.IsValid());
			this->AddPin(NewPin_2.ToSharedRef());
		}
		else
		{
			TSharedPtr<SGraphPin> NewPin = FNodeFactory::CreatePinWidget(CurrentPin);
			check(NewPin.IsValid());
			this->AddPin(NewPin.ToSharedRef());
		}
	}
}

EVisibility SGraphNode_K2GPN::IsAddPinButtonVisible() const
{
	return  EVisibility::Visible;
}

void SGraphNode_K2GPN::CreateOutputSideAddButton(TSharedPtr<SVerticalBox> OutputBox)
{
	TSharedRef<SWidget> AddPinButton = AddPinButtonContent(
		NSLOCTEXT("SwitchStatementNode", "SwitchStatementNodeAddPinButton", "Add pin"),
		NSLOCTEXT("SwitchStatementNode", "SwitchStatementNodeAddPinButton_Tooltip", "Well, didn't you expect it?"));

	FMargin AddPinPadding = FMargin(1, 1)/*Settings->GetOutputPinPadding()*/;
	AddPinPadding.Top += 6.0f;

	OutputBox->AddSlot()
		.AutoHeight()
		.VAlign(VAlign_Center)
		.Padding(AddPinPadding)
		[
			AddPinButton
		];
}

FReply SGraphNode_K2GPN::OnAddPin()
{
	UK2Node_GetPropName* GetPropNameNode = CastChecked<UK2Node_GetPropName>(GraphNode);
	if (GetPropNameNode && GetPropNameNode->CanAddPin())
	{
		GetPropNameNode->AddInputPin();
	}
	return SGraphNode::OnAddPin();
}
