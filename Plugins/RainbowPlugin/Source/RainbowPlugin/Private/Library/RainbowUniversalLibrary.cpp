// Fill out your copyright notice in the Description page of Project Settings.


#include "Library/RainbowUniversalLibrary.h"
#include "Misc/MessageDialog.h"

EMyAppReturnType URainbowUniversalLibrary::SendMessage(TEnumAsByte<EMyAppMsgType> Type, FText Message, FText Title)
{
	EAppMsgType::Type MessageType = EAppMsgType::Ok;
	switch (Type)
	{
	case Ok: MessageType = EAppMsgType::Ok; break;
	case YesNo: MessageType = EAppMsgType::YesNo; break;
	case OkCancel: MessageType = EAppMsgType::OkCancel; break;
	case YesNoCancel: MessageType = EAppMsgType::YesNoCancel; break;
	case CancelRetryContinue: MessageType = EAppMsgType::CancelRetryContinue; break;
	case YesNoYesAllNoAll: MessageType = EAppMsgType::YesNoYesAllNoAll; break;
	case YesNoYesAllNoAllCancel: MessageType = EAppMsgType::YesNoYesAllNoAllCancel; break;
	case YesNoYesAll:  MessageType = EAppMsgType::YesNoYesAll; break;
	default:;
	}
	const EAppReturnType::Type AppReturn = FMessageDialog::Open(MessageType, Message, &Title);
	switch (AppReturn)
	{
	case EAppReturnType::No: return EMyAppReturnType::No; break;
	case EAppReturnType::Yes: return EMyAppReturnType::Yes; break;
	case EAppReturnType::YesAll: return EMyAppReturnType::YesAll; break;
	case EAppReturnType::NoAll: return EMyAppReturnType::NoAll; break;
	case EAppReturnType::Cancel: return EMyAppReturnType::Cancel; break;
	case EAppReturnType::Ok: return EMyAppReturnType::OK; break;
	case EAppReturnType::Retry: return EMyAppReturnType::Retry; break;
	case EAppReturnType::Continue: return EMyAppReturnType::Continue; break;
	default: return None;
	}
}
