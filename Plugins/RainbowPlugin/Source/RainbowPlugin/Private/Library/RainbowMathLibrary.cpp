// Fill out your copyright notice in the Description page of Project Settings.


#include "Library/RainbowMathLibrary.h"

bool URainbowMathLibrary::NearlyEqual_FloatFloat(float A, float B, float ErrorTolerance)
{
	return !FMath::IsNearlyEqual(A, B, ErrorTolerance);
}

float URainbowMathLibrary::DoNegative_Float(const float A)
{
	return FMath::Abs(A) * (-1);
}

int32 URainbowMathLibrary::DoNegative_Int32(const int32 A)
{
	return FMath::Abs(A) * (-1);
}

int64 URainbowMathLibrary::DoNegative_Int64(const int64 A)
{
	return FMath::Abs(A) * (-1);
}

float URainbowMathLibrary::Flip_Float(float A)
{
	return A * (-1);
}
