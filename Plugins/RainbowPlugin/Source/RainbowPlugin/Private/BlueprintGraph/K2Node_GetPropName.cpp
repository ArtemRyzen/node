// Fill out your copyright notice in the Description page of Project Settings.

#include "BlueprintGraph/K2Node_GetPropName.h"
#include "BlueprintActionDatabaseRegistrar.h"
#include "BlueprintEditorSettings.h"
#include "BlueprintNodeSpawner.h"
#include "BPTerminal.h"
#include "EdGraphSchema_K2.h"
#include "EdGraphUtilities.h"
#include "KismetCompiler.h"
#include "RainbowPlugin/Public/KismetNodes/SGraphNode_K2GPN.h"

#define LOCTEXT_NAMESPACE "UK2Node_GetPropName"

namespace
{
	static const FName StructurePinName(TEXT("Structure"));
	static const FName ReturnPinName(TEXT("Value"));
	static const FName NamePropName(TEXT("NameProp"));
	static const FString Prefix(TEXT("_"));
}

class FKCHandler_GetPropName : public FNodeHandlingFunctor
{
public:
	FKCHandler_GetPropName(FKismetCompilerContext& InCompilerContext)
		: FNodeHandlingFunctor(InCompilerContext)
	{
	}

	FBPTerminal* RegisterInputTerm(FKismetFunctionContext& Context, UK2Node_GetPropName* Node)
	{
		check(NULL != Node);

		if (NULL == Node->StructType)
		{
			CompilerContext.MessageLog.Error(*LOCTEXT("BreakStruct_UnknownStructure_Error", "Unknown structure to break for @@").ToString(), Node);
			return NULL;
		}

		//Find input pin
		UEdGraphPin* InputPin = NULL;
		for (int32 PinIndex = 0; PinIndex < Node->Pins.Num(); ++PinIndex)
		{
			UEdGraphPin* Pin = Node->Pins[PinIndex];
			if (Pin && (EGPD_Input == Pin->Direction))
			{
				InputPin = Pin;
				break;
			}
		}
		check(NULL != InputPin);

		//Find structure source net
		UEdGraphPin* Net = FEdGraphUtilities::GetNetFromPin(InputPin);
		check(NULL != Net);

		FBPTerminal** FoundTerm = Context.NetMap.Find(Net);
		FBPTerminal* Term = FoundTerm ? *FoundTerm : NULL;
		if (NULL == Term)
		{
			// Dont allow literal
			if ((Net->Direction == EGPD_Input) && (Net->LinkedTo.Num() == 0))
			{
				CompilerContext.MessageLog.Error(*LOCTEXT("InvalidNoInputStructure_Error", "No input structure to break for @@").ToString(), Net);
				return NULL;
			}
			// standard register net
			else
			{
				Term = Context.CreateLocalTerminalFromPinAutoChooseScope(Net, Context.NetNameMap->MakeValidName(Net));
			}
			Context.NetMap.Add(Net, Term);
		}
		UStruct* StructInTerm = Cast<UStruct>(Term->Type.PinSubCategoryObject.Get());
		if (NULL == StructInTerm || !StructInTerm->IsChildOf(Node->StructType))
		{
			CompilerContext.MessageLog.Error(*LOCTEXT("BreakStruct_NoMatch_Error", "Structures don't match for @@").ToString(), Node);
		}
		return Term;
	}

	void RegisterOutputTerm(FKismetFunctionContext& Context, UScriptStruct* StructType, UEdGraphPin* Net, FBPTerminal* ContextTerm, UK2Node_GetPropName* Node)
	{
		//CompilerContext.MessageLog.Error(*LOCTEXT("InvalidNoInputStructure_Error", "Poshel naxuy, ya ne budu compilirovatsa").ToString());
		//UE_LOG(LogTemp, Log, TEXT("ValuePin: %s"), *Net->PinName.ToString())
		if (UProperty* BoundProperty = FindField<UProperty>(StructType, Node->PibValueName))
		{
			if (BoundProperty->HasAnyPropertyFlags(CPF_Deprecated) && Net->LinkedTo.Num())
			{
				FText Message = FText::Format(LOCTEXT("BreakStruct_DeprecatedField_Warning", "@@ : Member '{0}' of struct '{1}' is deprecated.")
					, BoundProperty->GetDisplayNameText()
					, StructType->GetDisplayNameText());
				CompilerContext.MessageLog.Warning(*Message.ToString(), Net->GetOuter());
			}

			UBlueprintEditorSettings* Settings = GetMutableDefault<UBlueprintEditorSettings>();
			FBPTerminal* Term = Context.CreateLocalTerminalFromPinAutoChooseScope(Net, Node->PibValueName.ToString());

			Term->bPassedByReference = ContextTerm->bPassedByReference;
			Term->AssociatedVarProperty = BoundProperty;
			Context.NetMap.Add(Net, Term);
			Term->Context = ContextTerm;

			if (BoundProperty->HasAnyPropertyFlags(CPF_BlueprintReadOnly))
			{
				Term->bIsConst = true;
			}
			UE_LOG(LogTemp, Log, TEXT("ValuePin: %s"), *Net->PinName.ToString())
		}
		else
		{
			CompilerContext.MessageLog.Error(TEXT("Failed to find a struct member for @@"), Net);
		}
	}

	virtual void RegisterNets(FKismetFunctionContext& Context, UEdGraphNode* InNode) override
	{
		UK2Node_GetPropName* Node = Cast<UK2Node_GetPropName>(InNode);
		check(Node);

		//if (!UK2Node_GetPropName::CanBeBroken(Node->StructType, Node->IsIntermediateNode()))
		//{
		//	CompilerContext.MessageLog.Warning(*LOCTEXT("BreakStruct_NoBreak_Error", "The structure cannot be broken using generic 'break' node @@. Try use specialized 'break' function if available.").ToString(), Node);
		//}

		if (FBPTerminal* StructContextTerm = RegisterInputTerm(Context, Node))
		{
			for (UEdGraphPin* Pin : Node->Pins)
			{
				if (Pin && EGPD_Output == Pin->Direction)
				{
					RegisterOutputTerm(Context, Node->StructType, Pin, StructContextTerm, Node);
				}
			}
		}
	}
};

FNodeHandlingFunctor* UK2Node_GetPropName::CreateNodeHandler(FKismetCompilerContext& CompilerContext) const
{
	return new FKCHandler_GetPropName(CompilerContext);
}

UK2Node_GetPropName::UK2Node_GetPropName()
{
	NumOptionPins = 1;
	OrphanedPinSaveMode = ESaveOrphanPinMode::SaveNone;
	IndexPinType.PinCategory = UEdGraphSchema_K2::PC_Wildcard;
	IndexPinType.PinSubCategory = UEdGraphSchema_K2::PSC_Index;
	IndexPinType.PinSubCategoryObject = nullptr;
}

bool UK2Node_GetPropName::CanAddPin() const
{
	if(NumOptionPins == StructName.Num())
	{
		return false;
	}
	return true;
}

void UK2Node_GetPropName::AddInputPin()
{
	NumOptionPins++;
	ReconstructNode();
}

//�������� ��������� �����
void UK2Node_GetPropName::AllocateDefaultPins()
{
	CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Wildcard, StructurePinName);
	NamePins.Empty();
	ReturnPins.Empty();
	//CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Wildcard, ReturnPinName);	
	//CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Name, NamePropName);
	for (int32 x = 0; x < NumOptionPins; x++)
	{
		const FName NameNameProp = *FString::Printf(TEXT("NameProp %d"), x);
		const FName NameReturnPin = *FString::Printf(TEXT("Value %d"), x);
		auto NamePin = CreatePin(EGPD_Input, UEdGraphSchema_K2::PC_Name, NameNameProp);
		auto ReturnPin = CreatePin(EGPD_Output, UEdGraphSchema_K2::PC_Wildcard, NameReturnPin);
		//if(NamePin && NamePin)
		NamePins.Add(NamePin);
		ReturnPins.Add(ReturnPin);
	}
	
	Super::AllocateDefaultPins();
}

FText UK2Node_GetPropName::GetNodeTitle(ENodeTitleType::Type TitleType) const
{
	if (TitleType == ENodeTitleType::ListView || TitleType == ENodeTitleType::MenuTitle)
	{
		return NSLOCTEXT("K2Node", "GetPropName_Title", "GetPropValue NONE");
	}
	else if(const auto St = GetStruct())
	{
		if (CachedNodeTitle.IsOutOfDate(this))
		{
			FFormatNamedArguments Args;
			Args.Add(TEXT("StructName"), St->GetDisplayNameText());
			// FText::Format() is slow, so we cache this to save on performance
			CachedNodeTitle.SetCachedText(FText::Format(GetNodeTitleFormat(), Args), this);
		}
		return CachedNodeTitle;
	}
	return NSLOCTEXT("K2Node", "ConstructObject_Title_NONE", "GetPropValue NONE");
}

void UK2Node_GetPropName::GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const
{
	// actions get registered under specific object-keys; the idea is that 
	// actions might have to be updated (or deleted) if their object-key is  
	// mutated (or removed)... here we use the node's class (so if the node 
	// type disappears, then the action should go with it)
	UClass* ActionKey = GetClass();
	// to keep from needlessly instantiating a UBlueprintNodeSpawner, first   
	// check to make sure that the registrar is looking for actions of this type
	// (could be regenerating actions for a specific asset, and therefore the 
	// registrar would only accept actions corresponding to that asset)
	if (ActionRegistrar.IsOpenForRegistration(ActionKey))
	{
		UBlueprintNodeSpawner* NodeSpawner = UBlueprintNodeSpawner::Create(GetClass());
		check(NodeSpawner != nullptr);

		ActionRegistrar.AddBlueprintAction(ActionKey, NodeSpawner);
	}
}

//��������� ����� ��� ����������� ������ �� ���
void UK2Node_GetPropName::NotifyPinConnectionListChanged(UEdGraphPin* Pin)
{
	Super::NotifyPinConnectionListChanged(Pin);
	if (Pin->LinkedTo.Num() > 0)
	{
		if (Pin == GetStructPin())
		{
			UEdGraphPin* LinkPin = Pin->LinkedTo[0];
			if (const auto Struct = Cast<UScriptStruct>(LinkPin->PinType.PinSubCategoryObject))
			{
				StructType = Struct;
				GetGraph()->NotifyGraphChanged();
				for (TFieldIterator<UProperty> It(Struct, EFieldIteratorFlags::IncludeSuper); It; ++It)
				{
					UProperty* TestProperty = *It;
					StructName.Add(TestProperty->GetFName());
					int32 Index = 0;
					for(auto GraphPin : NamePins)
					{				
						if (TestProperty->GetFName() == FName(*GraphPin->DefaultValue))
						{
							const UEdGraphSchema_K2* Schema = GetDefault<UEdGraphSchema_K2>();
							FEdGraphPinType DumbGraphPinType;
							if (Schema->ConvertPropertyToPinType(TestProperty, DumbGraphPinType))
							{
								GetReturnPin(Index)->PinType = DumbGraphPinType;
								GetReturnPin(Index)->PinFriendlyName = FText::FromString(TestProperty->GetCPPType());
								PibValueName = *GetValuePin()->GetDefaultAsString();
							}
						}
						Index++;
					}
				}
				UEdGraph* Graph = GetGraph();
				Graph->NotifyGraphChanged();
			}
			Pin->PinType = LinkPin->PinType;
		}
	}
	/*else
	{
		if (Pin == GetStructPin())
		{
			if(UEdGraphPin* ReturnPin = GetReturnPin())
			{
				ReturnPin->PinType.PinCategory = UEdGraphSchema_K2::PC_Wildcard;
				ReturnPin->PinType.PinSubCategory = NAME_None;
				ReturnPin->PinType.PinSubCategoryObject = nullptr;
				ReturnPin->PinFriendlyName = FText::FromName(ReturnPinName);
				ReturnPin->BreakAllPinLinks();
				StructName.Empty();
			}
			Pin->PinType.PinCategory = UEdGraphSchema_K2::PC_Wildcard;
			Pin->PinType.PinSubCategory = NAME_None;
			Pin->PinType.PinSubCategoryObject = nullptr;
			GetGraph()->NotifyGraphChanged();
		}
	}*/
}

void UK2Node_GetPropName::ReallocatePinsDuringReconstruction(TArray<UEdGraphPin*>& OldPins)
{
	Super::ReallocatePinsDuringReconstruction(OldPins);

	TArray<UEdGraphPin*> OldStructPins;
	TArray<UEdGraphPin*> OldValuePins;
	UEdGraphPin* OldStructurePins = nullptr;
	for (UEdGraphPin* OldPin : OldPins)
	{
		for(auto NP : NamePins)
		{
			if(NP->PinName == OldPin->PinName)
			{
				OldValuePins.Add(OldPin);
			}
		}
		for (auto NP : ReturnPins)
		{
			if (NP->PinName == OldPin->PinName)
			{
				OldStructPins.Add(OldPin);
			}
		}
		if(OldPin->PinName == GetStructPin()->PinName)
		{
			OldStructurePins = OldPin;
		}
	}

	TArray<UEdGraphPin*> NewNamePins = NamePins;
	TArray<UEdGraphPin*> NewReturnPins = ReturnPins;
	UEdGraphPin* NewStruntPin = GetStructPin();

	if(OldStructurePins && IsWildcardPin(NewStruntPin))
	{
		NewStruntPin->PinType = OldStructurePins->PinType;
	}
	
	for (int32 NP = 0; NP < NewNamePins.Num(); NP++)
	{
		if(OldStructPins.IsValidIndex(NP) && IsWildcardPin(NewReturnPins[NP]))
		{
			NewReturnPins[NP]->PinType = OldStructPins[NP]->PinType;
			NewReturnPins[NP]->PinFriendlyName = OldStructPins[NP]->PinFriendlyName;
		}
		if(OldValuePins.IsValidIndex(NP) && (NewNamePins[NP]->PinType.PinCategory == UEdGraphSchema_K2::PC_Wildcard))
		{
			NewNamePins[NP]->PinType = OldValuePins[NP]->PinType;
		}
	}
	
	CachedNodeTitle.MarkDirty();
}

void UK2Node_GetPropName::PinDefaultValueChanged(UEdGraphPin* Pin)
{
	if(NamePins.Contains(Pin))
	{
		int32 Index = 0;
		for(auto E : NamePins)
		{
			if(E == Pin)
			{
				break;
			}
			Index++;
		}
		GetGraph()->NotifyGraphChanged();
		for (TFieldIterator<UProperty> It(StructType, EFieldIteratorFlags::IncludeSuper); It; ++It)
		{
			UProperty* TestProperty = *It;
			if (TestProperty->GetFName() == FName(*Pin->DefaultValue))
			{
				const UEdGraphSchema_K2* Schema = GetDefault<UEdGraphSchema_K2>();
				FEdGraphPinType DumbGraphPinType;
				if (Schema->ConvertPropertyToPinType(TestProperty, DumbGraphPinType))
				{
					//LFlag = false;
					GetReturnPin(Index)->PinType = DumbGraphPinType;
					GetReturnPin(Index)->PinFriendlyName = FText::FromString(TestProperty->GetNameCPP());
					PibValueName = *Pin->GetDefaultAsString();
				}
			}
		}
	}
	else if (Pin == GetStructPin())
	{
		UEdGraph* Graph = GetGraph();
		Graph->NotifyGraphChanged();
		CachedNodeTitle.MarkDirty();
	}
	
	/*bool LFlag = true;
	if (Pin == GetValuePin())
	{	
		if (const auto Struct = Cast<UScriptStruct>(GetStructPin()->PinType.PinSubCategoryObject))
		{		
			StructType = Struct;
			GetGraph()->NotifyGraphChanged();
			for (TFieldIterator<UProperty> It(Struct, EFieldIteratorFlags::IncludeSuper); It; ++It)
			{
				UProperty* TestProperty = *It;
				if (TestProperty->GetFName() == FName(*GetValuePin()->DefaultValue))
				{
					const UEdGraphSchema_K2* Schema = GetDefault<UEdGraphSchema_K2>();
					FEdGraphPinType DumbGraphPinType;
					if (Schema->ConvertPropertyToPinType(TestProperty, DumbGraphPinType))
					{
						LFlag = false;
						GetReturnPin()->PinType = DumbGraphPinType;
						GetReturnPin()->PinFriendlyName = FText::FromString(TestProperty->GetCPPType());
						PibValueName = *GetValuePin()->GetDefaultAsString();
					}
				}
			}
			if(LFlag)
			{
				GetReturnPin()->PinType.PinCategory = UEdGraphSchema_K2::PC_Wildcard;
				GetReturnPin()->PinFriendlyName = FText::FromName(ReturnPinName);
				GetReturnPin()->BreakAllPinLinks();	
			}
		}
	}
	else if(Pin == GetStructPin())
	{	
		UEdGraph* Graph = GetGraph();
		Graph->NotifyGraphChanged();
		CachedNodeTitle.MarkDirty();
	}*/
}

void UK2Node_GetPropName::PreloadRequiredAssets()
{
	PreloadObject(StructType);
	Super::PreloadRequiredAssets();
}

void UK2Node_GetPropName::ValidateNodeDuringCompilation(FCompilerResultsLog& MessageLog) const
{
	Super::ValidateNodeDuringCompilation(MessageLog);

	if (!StructType)
	{
		MessageLog.Error(*LOCTEXT("NoStruct_Error", "No Struct in @@").ToString(), this);
	}
}

TSharedPtr<SGraphNode> UK2Node_GetPropName::CreateVisualWidget()
{
	return SNew(SGraphNode_K2GPN, this);
}

UEdGraphPin* UK2Node_GetPropName::GetReturnPin(int32 IIndex) const
{
	if(ReturnPins.IsValidIndex(IIndex))
	{
		return ReturnPins[IIndex];
	}
	return nullptr;
	/*UEdGraphPin* Pin = FindPinChecked(ReturnPinName);
	check(Pin !=nullptr);
	return Pin;*/
}

UEdGraphPin* UK2Node_GetPropName::GetValuePin() const
{
	UEdGraphPin* Pin = FindPinChecked(NamePropName);
	check(Pin != nullptr);
	return Pin;
}

TArray<UEdGraphPin*> UK2Node_GetPropName::GetAllNamePin() const
{
	return NamePins;
}

UEdGraphPin* UK2Node_GetPropName::GetStructPin() const
{
	UEdGraphPin* Pin = FindPinChecked(StructurePinName);
	check(Pin != nullptr);
	return Pin;
}

UScriptStruct* UK2Node_GetPropName::GetStruct() const
{
	if(const auto Struct = Cast<UScriptStruct>(GetStructPin()->PinType.PinSubCategoryObject))
	{
		return Struct;
	}
	return nullptr;
}

bool UK2Node_GetPropName::IsWildcardPin(UEdGraphPin* IPin)
{
	return IPin->PinType.PinCategory == UEdGraphSchema_K2::PC_Wildcard;
}

FText UK2Node_GetPropName::GetNodeTitleFormat() const
{
	return NSLOCTEXT("UK2Node_GetPropName", "Construct", "GetPropValue by {StructName}");
}

#undef LOCTEXT_NAMESPACE