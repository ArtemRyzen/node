// Fill out your copyright notice in the Description page of Project Settings.
#pragma once

#include "CoreMinimal.h"
#include "EdGraph/EdGraphNodeUtils.h"
#include "K2Node.h"
#include "K2Node_AddPinInterface.h"
#include "K2Node_StructOperation.h"
#include "K2Node_GetPropName.generated.h"

class FBlueprintActionDatabaseRegistrar;

/**
 * 
 */
UCLASS()
class RAINBOWPLUGIN_API UK2Node_GetPropName : public UK2Node, public IK2Node_AddPinInterface
{
	GENERATED_BODY()

protected:
	//~ Begin UEdGraphNode Interface.
	virtual void AllocateDefaultPins() override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	virtual void GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const override;
	virtual void NotifyPinConnectionListChanged(UEdGraphPin* Pin) override;
	virtual bool IsNodeSafeToIgnore() const override { return true; }
	virtual void ReallocatePinsDuringReconstruction(TArray<UEdGraphPin*>& OldPins) override;
	virtual void PinDefaultValueChanged(UEdGraphPin* Pin) override;
	virtual void PreloadRequiredAssets() override;
	virtual void ValidateNodeDuringCompilation(class FCompilerResultsLog& MessageLog) const override;
	virtual TSharedPtr<SGraphNode> CreateVisualWidget() override;
	//~ End UEdGraphNode Interface.

	//~ Begin UK2Node Interface.
	virtual bool IsNodePure() const override { return true; }
	virtual class FNodeHandlingFunctor* CreateNodeHandler(class FKismetCompilerContext& CompilerContext) const override;
	virtual bool CanEverRemoveExecutionPin() const override { return true; }
	//~ End UK2Node Interface.
	
	UK2Node_GetPropName();

public:

	//~ IK2Node_AddPinInterface
	virtual bool CanAddPin() const override;
	virtual void AddInputPin() override;
	//~ End IK2Node_AddPinInterface

	UPROPERTY()
	UScriptStruct* StructType;

	UPROPERTY()
	FName PibValueName;

	UPROPERTY()
	int32 NumOptionPins;

	UPROPERTY()
	TArray<FName> StructName;

	TArray<UEdGraphPin*> NamePins;
	TArray<UEdGraphPin*> ReturnPins;
	/** The pin type of the index pin */
	UPROPERTY()
	FEdGraphPinType IndexPinType;

	UEdGraphPin* GetReturnPin(int32 IIndex) const;

	UEdGraphPin* GetValuePin() const;
	
	TArray<UEdGraphPin*> GetAllNamePin() const;
	
	UEdGraphPin* GetStructPin() const;
	
	virtual FText GetNodeTitleFormat() const;

	UScriptStruct* GetStruct() const;

	bool IsWildcardPin(UEdGraphPin* IPin);
	
	/** Constructing FText strings can be costly, so we cache the node's title */
	FNodeTextCache CachedNodeTitle;
};