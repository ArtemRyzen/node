// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/ObjectMacros.h"
#include "EdGraph/EdGraphPin.h"
#include "K2Node_Switch.h"
#include "K2Node_SwitchFloat.generated.h"

class FBlueprintActionDatabaseRegistrar;

USTRUCT(BlueprintType)
struct FNearlyQual
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float Value;

	UPROPERTY(EditAnywhere)
	float ErrorTolerance;

	FNearlyQual()
	{
		Init();
	}

	explicit FNearlyQual(float InValue, float InErrorTolerance)
	{
		Init();
		Value = InValue;
		ErrorTolerance = InErrorTolerance;
	}

	//Function
	FORCEINLINE void Init()
	{
		Value = 0.f;
		ErrorTolerance = 0.000001f;
	}
};

/**
 * 
 */
UCLASS()
class RAINBOWPLUGIN_API UK2Node_SwitchFloat : public UK2Node_Switch
{
	GENERATED_BODY()

protected:
	UK2Node_SwitchFloat(const FObjectInitializer& ObjectInitializer);

public:
	
	UPROPERTY(EditAnywhere, Category = "FloatOptions")
	float DefaultErrorTolerance = 0.000001f;

	UPROPERTY(EditAnywhere, Category = "PinOptions")
	TArray<FNearlyQual> PinNameValue;
	
private:
	
	UPROPERTY(EditAnywhere, Category = "Design")
	FLinearColor NodeTittleColor = FLinearColor(255.0f, 0.f, 255.0f);

	// UObject interface
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& PropertyChangedEvent) override;
	// End of UObject interface

public:
	
	virtual FName GetPinNameGivenIndex(int32 Index) const override;

	// UEdGraphNode interface
	virtual FText GetTooltipText() const override;
	virtual FText GetNodeTitle(ENodeTitleType::Type TitleType) const override;
	virtual FLinearColor GetNodeTitleColor() const override;
	virtual bool ShouldShowNodeProperties() const override { return true; }
	// End of UEdGraphNode interface

	// UK2Node_Switch Interface
	virtual FEdGraphPinType GetPinType() const override;
	virtual void AddPinToSwitchNode() override;
	// End of UK2Node_Switch Interface

	// UK2Node interface
	virtual void GetMenuActions(FBlueprintActionDatabaseRegistrar& ActionRegistrar) const override;
	virtual class FNodeHandlingFunctor* CreateNodeHandler(class FKismetCompilerContext& CompilerContext) const override;
	// End of UK2Node interface

protected:
	
	virtual void CreateFunctionPin() override;
	virtual void CreateSelectionPin() override;
	virtual void CreateCasePins() override;

};
