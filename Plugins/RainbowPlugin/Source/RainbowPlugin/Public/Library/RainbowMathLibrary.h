// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/KismetMathLibrary.h"
#include "RainbowMathLibrary.generated.h"

/**
 * 
 */
UCLASS()
class RAINBOWPLUGIN_API URainbowMathLibrary : public UObject
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Nearly Equal (float)", Keywords = "== equal"), Category = "RainbowMath|Float")
	static bool NearlyEqual_FloatFloat(float A, float B, float ErrorTolerance = 1.e-6f);

	/** Makes the float number negative */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "DoNegativeNumber (float)", Keywords = "*-1"), Category = "RainbowMath|Float")
	static float DoNegative_Float(float A);

	/** Makes the int32 number negative */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "DoNegativeNumber (int32)", Keywords = "*-1"), Category = "RainbowMath|Float")
	static int32 DoNegative_Int32(int32 A);

	/** Makes the int64 number negative */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "DoNegativeNumber (int64)", Keywords = "*-1"), Category = "RainbowMath|Float")
	static int64 DoNegative_Int64(int64 A);

	/** Reverses the sign of the number */
	UFUNCTION(BlueprintPure, meta = (DisplayName = "FlipNumber (float)", CompactNodeTitle = "~", Keywords = "*-1"), Category = "RainbowMath|Float")
	static float Flip_Float(float A);

};
