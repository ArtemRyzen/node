// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "RainbowUniversalLibrary.generated.h"

UENUM(BlueprintType)
enum EMyAppMsgType
{
	Ok				       UMETA(DisplayName = "Ok"),
	YesNo			       UMETA(DisplayName = "YesNo"),
	OkCancel		       UMETA(DisplayName = "OkCancel"),
	YesNoCancel            UMETA(DisplayName = "YesNoCancel"),
	CancelRetryContinue    UMETA(DisplayName = "CancelRetryContinue"),
	YesNoYesAllNoAll       UMETA(DisplayName = "YesNoYesAllNoAll"),
	YesNoYesAllNoAllCancel UMETA(DisplayName = "YesNoYesAllNoAllCancel"),
	YesNoYesAll            UMETA(DisplayName = "YesNoYesAll"),
};

UENUM(BlueprintType)
enum EMyAppReturnType
{
	None		UMETA(DisplayName = "None"),
	No			UMETA(DisplayName = "No"),
	Yes			UMETA(DisplayName = "Yes"),
	YesAll		UMETA(DisplayName = "YesAll"),
	NoAll		UMETA(DisplayName = "NoAll"),
	Cancel		UMETA(DisplayName = "Cancel"),
	OK			UMETA(DisplayName = "Ok"),
	Retry		UMETA(DisplayName = "Retry"),
	Continue	UMETA(DisplayName = "Continue"),
};


/**
 * 
 */
UCLASS()
class RAINBOWPLUGIN_API URainbowUniversalLibrary : public UObject
{
	GENERATED_BODY()
	
	UFUNCTION(BlueprintCallable, Category = "RainbowPlugin|GameMessage", meta = (DisplayName = "ShowMessage"))
	static EMyAppReturnType SendMessage(TEnumAsByte<EMyAppMsgType> Type, FText Message, FText Title);
};
