#pragma once

#include "CoreMinimal.h"
#include "KismetNodes/SGraphNodeK2Default.h"

class SGraphNode_K2GPN : public SGraphNodeK2Default
{
	virtual void CreatePinWidgets() override;
	virtual EVisibility IsAddPinButtonVisible() const override;
	virtual void CreateOutputSideAddButton(TSharedPtr<SVerticalBox> OutputBox) override;
	virtual FReply OnAddPin() override;
};